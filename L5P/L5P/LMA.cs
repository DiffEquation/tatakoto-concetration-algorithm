using System;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;

namespace Fit.LMA
{
	public class LMA
    {
        #region class members
        //interface of the function to be fitted
		private ILMAFunction function;
        //The vector of fit parameters
        private Vector<double> parameters;
        //Measured data points for which the model function is to be fitted.
		private Vector<double> X;
        private Vector<double> Y;
		//Weights for each data point. The merit function is: chi2 = sum[ (y_i - y(x_i;a))^2 * w_i ].
		//For gaussian errors in datapoints, set w_i = 1 / sigma_i.
		private Matrix<double> W;
		private double lambda = 0.001;	
		// default end conditions
		private double minDeltaChi2;
		private int maxIterations;
        private double finalChi2;
        private int iterationUsed;
        // вспомогательные переменные
        MatrixBuilder<double> MB = Matrix<double>.Build;
        VectorBuilder<double> VB = Vector<double>.Build;
        #endregion

        /// <summary>
		///In the LMA fit N is the number of data points, M is the number of fit parameters.
		///Call <code>fit()</code> to start the actual fitting.
		/// </summary>
		/// <param name="function">The model function to be fitted. Must be able to take M input parameters.</param>
        /// /// <param name="dataPointsX">The data points X in an array.</param>
        /// /// <param name="dataPointsY">The data points Y in an array.</param>
		/// <param name="parameters">The initial guess for the fit parameters, length M.</param>
		/// <param name="weights">The weights, normally given as: <code>weights[i] = 1 / sigma_i^2</code>. 
		/// If you have a bad data point, set its weight to zero. If the given array is null,
		/// a new array is created with all elements set to 1.</param>
        /// <param name="argDeltaChi2">delta chi square</param>
        /// <param name="argMaxIter">maximum number of iterations</param>
		public LMA(LMAFunction function, double[] dataPointsX, double[] dataPointsY, 
            double[] parameters, double[] weights, double argDeltaChi2, int argMaxIter) 
		{
			if (dataPointsX.Length != dataPointsY.Length) 
				throw new ArgumentException("Data must have the same number of x and y points.");	
			this.function = function;
			this.parameters = VB.DenseOfArray(parameters);
			X = VB.DenseOfArray(dataPointsX);
            Y = VB.DenseOfArray(dataPointsY);
			SetWeightsMatrix(dataPointsX.Length, weights);
			minDeltaChi2 = argDeltaChi2;
			maxIterations = argMaxIter;
		}

		#region Accessors
		public double[] Parameters
		{
			get{return parameters.ToArray();}
		}
        
		public int Iterations
		{
			get{return iterationUsed;}
		}

		public double Chi2
		{
			get{return finalChi2;}
		}
        
		#endregion

		public void Fit() 
		{
            int iterationCount = 0;
            // residuals dy = y - f(x; a)
            Vector<double> dy = CalculateResiduals(parameters);
            // Console.WriteLine(dy);
            // error 
            double chi2 = CalculateChi2(parameters);
            // Gradient Matrix
            Matrix<double> J = CalculateGradient(parameters);
            // Console.WriteLine(J);
            // Hessian H = J^T * W * J
            Matrix<double> H = J.Transpose() * W * J;
            // Console.WriteLine(H);
            // rhs of eqution beta = J^T * W * dy
            Vector<double> beta = J.Transpose() * W * dy;
            // Console.WriteLine(beta);
            bool is_parameters_corrected = false;
            
            while(iterationCount < maxIterations) 
			{
                if (is_parameters_corrected)
                {
                    dy = CalculateResiduals(parameters);
                    chi2 = CalculateChi2(parameters);
                    J = CalculateGradient(parameters);
                    // rhs of the equation
                    beta = J.Transpose() * W * dy;
                    H = J.Transpose() * W * J;
                }
                // copy and add lamda requalization term
                Matrix<double> H_lambda = MB.DenseOfMatrix(H);
                for (int i = 0; i < H_lambda.RowCount; i++)
                    H_lambda[i, i] *= 1 + lambda;
                // parameters correction a^\prime = a + da
                Vector<double> da = H_lambda.Solve(beta);
                double incrementedChi2 = CalculateChi2(parameters + da);
				if (incrementedChi2 >= chi2) 
				{
					lambda *= 2;
                    is_parameters_corrected = false;
				}
				else 
				{
					lambda /= 2;
                    parameters += da;
                    is_parameters_corrected = true;
				}
				iterationCount++;
                if (System.Math.Abs(incrementedChi2 - chi2) < minDeltaChi2)
                    break;
            };
            finalChi2 = chi2;
            iterationUsed = iterationCount;
		}
	
        /// <summary>
        /// Calculates gradient matrix for given parameter vector
        /// </summary>
        /// <param name="a">input parameters</param>
        /// <returns>Gradient matrix</returns>
        protected Matrix<double> CalculateGradient(Vector<double> a)
        {
            Matrix<double> J = MB.Dense(X.Count, a.Count);
            for (int i = 0; i < a.Count; i++)
            {
                J.SetColumn(i, function.GetPartialDerivative(X, a, i));
            }
            return J;
        }

        /// <summary>
        /// Calculates residuals dy = y - f(x; a)
        /// </summary>
        /// <param name="a">input parameters</param>
        /// <returns></returns>
        protected Vector<double> CalculateResiduals(Vector<double> a)
        {
            return Y - function.GetY(X, a);
        }

		/// <summary>
		/// Calculates value of the function for given parameter vector
		/// </summary>
		/// <param name="a">input parameters</param>
		/// <returns>value of the function</returns>
		protected double CalculateChi2(Vector<double> a) 
		{
            Vector<double> dy = CalculateResiduals(a);
            return dy * (W * dy);
		}
	
		/// <summary>
		/// Checks if the matrix of weights for each point is a 
		/// matrix of positive elements. Otherwise it initializes
		/// a new matrix and sets each value to 1
		/// </summary>
		/// <param name="length"></param>
		/// <param name="weights"></param>
		/// <returns></returns>
		protected void SetWeightsMatrix(int length, double[] weights) 
		{
			bool damaged = false;
			// check for null
			if (weights == null) 
			{
				damaged = true;
			}
				// check if all elements are zeros or if there are negative, NaN or Infinite elements
			else 
			{
				bool allZero = true;
				bool illegalElement = false;
				for (int i = 0; i < weights.Length && !illegalElement; i++) 
				{
					if (weights[i] < 0 || Double.IsNaN(weights[i]) || Double.IsInfinity(weights[i])) illegalElement = true;
					allZero = (weights[i] == 0) && allZero;
				}
				damaged = allZero || illegalElement;
			}

            if (damaged)
            {
                W = MB.DenseIdentity(length);
            }
            else
            {
                W = MB.DenseOfDiagonalArray(weights); 
            }
			return;
		}
	}
}
