using System;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;

namespace Fit.LMA
{
	/// <summary>
	/// Abstract class implementing the LMAFunction interface
	/// </summary>
	public abstract class LMAFunction : ILMAFunction
	{
        // вспомогательные переменные
        public MatrixBuilder<double> MB = Matrix<double>.Build;
        public VectorBuilder<double> VB = Vector<double>.Build;
		/// <summary>
		/// Returns the y value of the function for
		/// the given x and vector of parameters
		/// </summary>
		/// <param name="x">The <i>x</i>-value for which the <i>y</i>-value is calculated.</param>
		/// <param name="a">The fitting parameters. </param>
		/// <returns></returns>
		public abstract Vector<double> GetY(Vector<double> x, Vector<double> a);

		/// <summary>
		/// The method which gives the partial derivates used in the LMA fit.
		/// If you can't provide the functional derivative, use a small <code>a</code>-step (e.g., <i>da</i> = 1e-20)
		/// and return <i>dy/da</i> at the given <i>x</i> for each fit parameter.
		/// This is provided in the method below as a default implementation
		/// </summary>
		/// <param name="x">The <i>x</i>-value for which the partial derivate is calculated.</param>
		/// <param name="a">The fitting parameters.</param>
		/// <param name="parameterIndex">The parameter index for which the partial derivate is calculated.</param>
		/// <returns>The partial derivative of the function with respect to parameter <code>parameterIndex</code> at <i>x</i>.</returns>
		public abstract Vector<double> GetPartialDerivative(Vector<double> x, Vector<double> a, int parameterIndex); 
	}
}
