﻿using System;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using Fit.LMA;

namespace L5P
{
    public class L5PFunction : LMAFunction
    {
        /// <summary>
        /// Function f(x; a) = D + (A - D) /(1 + (x / C)^B)^E
        /// parameters vector a = [A, B, C, D, E]
        /// </summary>
        /// <param name="x"></param>
        /// <param name="a"></param>
        /// <returns></returns>

        public override Vector<double> GetY(Vector<double> x, Vector<double> a)
        {
            double A = a[0];
            double B = a[1];
            double C = a[2];
            double D = a[3];
            double E = a[4];
            Vector<double> fval = VB.Dense(x.Count);
            for (int i = 0; i < x.Count; i++)
            {
                double t = x[i] / C;
                t = Math.Pow(t, B);
                t = Math.Pow(1 + t, E);
                fval[i] = D + (A - D) / t;
            }
            return fval;
        }

        public override Vector<double> GetPartialDerivative(Vector<double> x, Vector<double> a, int parameterIndex)
        {
            Vector<double> fval = VB.Dense(x.Count);
            double A = a[0];
            double B = a[1];
            double C = a[2];
            double D = a[3];
            double E = a[4];

            switch (parameterIndex)
            {
                case 0:
                    for (int i = 0; i < x.Count; i++)
                    {
                        double t = Math.Pow(x[i] / C, B);
                        fval[i] = Math.Pow(1.0 + t, -E);
                    }
                    break;
                case 1:
                    for (int i = 0; i < x.Count; i++)
                    { 
                        double t1 = Math.Pow(x[i] / C, B);
                        double t2 = Math.Pow(1 + t1, E + 1);
                        fval[i] = E * (D - A) / t2 * t1 * Math.Log(x[i] / C);
                        if (x[i] == 0)
                            fval[i] = 0;
                    }
                    break;
                case 2:
                    for (int i = 0; i < x.Count; i++)
                    {
                        double t1 = Math.Pow(x[i] / C, B);
                        double t2 = Math.Pow(1 + t1, E + 1);
                        fval[i] = B * (A - D) * E / C / t2 * t1;
                    }
                    break;
                case 3:
                    for (int i = 0; i < x.Count; i++)
                    {
                        double t = Math.Pow(x[i] / C, B);
                        t = Math.Pow(1 + t, E);
                        fval[i] = 1 - 1 / t;
                    }
                    break;
                case 4:
                    for (int i = 0; i < x.Count; i++)
                    {
                        double t1 = 1 + Math.Pow(x[i] / C, B);
                        double t2 = Math.Pow(t1, E);
                        fval[i] = (D - A) / t2 * Math.Log(t1);
                    }
                    break;
                default:
                    throw new ArgumentException("No such parameter index: " + parameterIndex);
            }
            return fval;
        }
    }

    public class L5P
    {
        private LMAFunction func = new L5PFunction();
        private LMA fit_algorithm = null;

        public double[] Parameters { 
            get {
                if (fit_algorithm != null)
                    return fit_algorithm.Parameters;
                return null;
            } 
        }
        public double Chi2 { 
            get {
                if(fit_algorithm != null)
                    return fit_algorithm.Chi2;
                return 0.0;
            } 
        }
        public int IterationNumber { 
            get { 
                if(fit_algorithm != null)
                    return fit_algorithm.Iterations;
                return 0;
            } 
        }

        public L5P()
        {
        }

        public void Fit(double[] x, double[] y, double argDeltaChi2=0.001, int argMaxIter=100)
        {
            if (x.Length != y.Length)
                throw new ArgumentException("Data must have the same number of x and y points.");
            for (int i = 0; i < x.Length; i++)
            {
                if (Double.IsNaN(x[i]) || Double.IsNaN(y[i]) || Double.IsInfinity(x[i]) || Double.IsInfinity(y[i]))
                    throw new ArgumentException("There are NaNs or Infs in data.");
            }
            if (x.Length < 5)
                throw new ArgumentException("Not enough Data points");
            double[] init_param = get_initial_parameters(x, y);
            fit_algorithm = new LMA(func, x, y, init_param, null, argDeltaChi2, argMaxIter);
            fit_algorithm.Fit();
        }

        double[] get_initial_parameters(double[] x, double[] y)
        {
            // set the starting points:
            // A is the lower asymptote so guess it with min(y)
            // B is the Hill's slope so guess it with the slope of the line between first and last point.
            // C is the inflection point (the concentration of analyte where you have
            // half of the max response) so guess it finding the concentration whose
            // response is nearest to the mid response.
            // D is the upper asymptote so guess it with max(y)
            // E is the asymmetric factor and so guess it with no asymmetry (E=1).
            Array.Sort(x, y);
            double slope = (y[y.Length - 1] - y[0]) / (x[x.Length - 1] - x[0]);
            double max_y = max(y);
            double min_y = min(y);
            double averg_y = (max_y + min_y) / 2.0;
            double t = double.PositiveInfinity;
            int idx = -1;
            for (int i = 0; i < y.Length; i++)
            {
                double tmp = Math.Abs(y[i] - averg_y);
                if (tmp < t)
                {
                    t = tmp;
                    idx = i;
                }
            }
            double[] parameters = new double[5];
            parameters[0] = min(y);                     // A
            parameters[1] = (slope >= 0) ? 1.0 : -1.0;  // B
            parameters[2] = x[idx];                     // C
            parameters[3] = max(y);                     // D
            parameters[4] = 1.0;                        // E
            // Parameter C cannot be zero
            if (Math.Abs(parameters[2]) <= Double.Epsilon)
                parameters[2] = 1.0;
            return parameters;
        }

        double min(double[] x)
        {
            double t = double.PositiveInfinity;
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] < t)
                    t = x[i];
            }
            return t;
        }

        double max(double[] x)
        {
            double t = double.NegativeInfinity;
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] > t)
                    t = x[i];
            }
            return t;
        }

    }

    public class Program
    {
        static void Main(string[] args)
        {
            double[] x1 = { 0.0, 4.5, 10.6, 19.7, 40.0, 84.0, 210.0 };
            double[] y1 = { 0.0089, 0.0419, 0.0873, 0.2599, 0.7074, 1.528, 2.7739 };
            double[] x2 = {0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
            double[] y2 = {2.0049, 2.0820, 2.1833, 2.2551, 2.3318, 2.3822, 2.4259, 2.4851, 2.5187, 2.5519};
            string[] parameters_names = { "A", "B", "C", "D", "E" };
            L5P lp = new L5P();
            lp.Fit(x2, y2);
            for (int i = 0; i < lp.Parameters.Length; i++)
                Console.WriteLine("{0} = {1}", parameters_names[i], lp.Parameters[i]);
            Console.WriteLine("Chi2: {0}", lp.Chi2);
            Console.WriteLine("iteration: {0}", lp.IterationNumber);

            Console.WriteLine("press any key to exit");
            Console.ReadLine();
        }
    }
}
