clear 
% load algorithm dll
NET.addAssembly([pwd, '\L5P\L5P\bin\Debug\L5P.dll']);
l5p = L5P.L5P;
A = 2;
B = 2;
C = 20;
D = 1;
E = -1/10;

ff = @(x, p) p(4) + (p(1) - p(4)) ./ ((1 + (x / p(3)).^p(2)).^p(5));
func = @(x)D + (A - D)./((1 + (x / C).^B).^E);

x = linspace(0, 100, 100);
y = func(x) + random('norm', 0, 0.01, size(x));

[cf, G] = L5P(x,y);
disp(cf)

l5p.Fit(x, y);
p = double(l5p.Parameters);
disp('C# fitted parameters')
disp(p)
disp('sse')
disp(double(l5p.Chi2))
%%

plot(x, func(x))
hold on
plot(x, y, 'o')
plot(x, ff(x, p), 'g')
plot(x, cf(x), 'r')
hold off
legend('theory', 'noise data', 'C# fitting', 'MATLAB fitting','Location','southeast')
